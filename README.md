# README #

Esta aplicación muestra una serie de frases "graciosas" con una imagen de su autor asociada. Las imágenes y frases se obtienen de archivos ubicados en el servidor local del ordenador.
Para su correcta ejecución es necesario añadir las siguientes dependencias al gradle: 
  compile 'cz.msebera.android:httpclient:4.4.1.2' 
  compile 'com.loopj.android:android-async-http:1.4.9' 
  compile 'com.squareup.picasso:picasso:2.5.2' 
  compile 'com.android.support:design:24.2.1'

Tambíen es necesario de disponer del script en php para llevar un registro de los errores http que puedan ocurrir durante su ejecución. El script esta incluido en la carpeta raíz del proyecto