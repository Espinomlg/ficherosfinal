package com.example.espino.trabajoficheros.fileMethods;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;


public class InputMethods {

    private Context context;

    public InputMethods(Context context){
        this.context = context;
    }

    public String readRaw(String fichero) {
        String result = "";
        BufferedReader br = null;

        try {
            br = new BufferedReader(new InputStreamReader(context.getResources().openRawResource(context.getResources().getIdentifier(fichero, "raw", context.getPackageName()))));
            result = br.readLine();


        } catch (Exception e) {
            Log.e("Error", e.getMessage());
        } finally {
            try {
                if (br != null) {
                    br.close();
                }
            } catch (Exception e) {
                Log.e("Error al cerrar", e.getMessage());
            }
        }
        return result;
    }
}
