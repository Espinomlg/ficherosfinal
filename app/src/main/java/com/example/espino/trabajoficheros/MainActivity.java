package com.example.espino.trabajoficheros;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.CountDownTimer;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.espino.trabajoficheros.fileMethods.InputMethods;
import com.example.espino.trabajoficheros.models.RestClient;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Date;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {

    private static final int IMAGES_FILE = 0;
    private static final int SENTENCES_FILE = 1;
    private static final String INTERVAL = "interval";
    private static final String ERRORS = "192.168.1.10/acdat/TrabajoFicheros/addLineToFile.php";

    private TextInputLayout tilImagesFile,
            tilSentencesFile;
    private ImageView img;
    private TextView sentence;

    private ArrayList<String> imagesList,
            sentecesList;
    private InputMethods input;
    private int index;
    private boolean downloadedImages;
    private boolean downloadedSentences;
    private boolean running;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(!isNetworkAvailable()){
            AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
            dialog.setTitle("Error:");
            dialog.setMessage("There is no connection to internet, the application will be closed");
            dialog.setPositiveButton("Close", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    finish();
                }
            });
        }

        tilImagesFile = (TextInputLayout) findViewById(R.id.main_til_images);
        tilSentencesFile = (TextInputLayout) findViewById(R.id.main_til_sentences);
        img = (ImageView) findViewById(R.id.main_imv_image);
        sentence = (TextView) findViewById(R.id.main_txv_sentence);

        imagesList = new ArrayList<>();
        sentecesList = new ArrayList<>();
        input = new InputMethods(MainActivity.this);
        index = 0;
        downloadedImages = false;
        downloadedSentences = false;
        running = false;
    }

    public void start(View view) {

        if (view.getId() == R.id.main_btn_visualize && !running && (downloadedImages && downloadedSentences)) {
            running = true;
            new MyCountDownTimer().start();
        }
        else if(view.getId() == R.id.main_btn_download){
            String imagesUrl = tilImagesFile.getEditText().getText().toString();
            String sentecesUrl = tilSentencesFile.getEditText().getText().toString();
            if (!TextUtils.isEmpty(imagesUrl) && !TextUtils.isEmpty(sentecesUrl)) {
                download(imagesUrl, IMAGES_FILE);
                download(sentecesUrl, SENTENCES_FILE);
            }
        }
    }

    public void download(String url, final int content) {
        final ProgressDialog progress = new ProgressDialog(this);

        RestClient.get(url, new TextHttpResponseHandler() {

            @Override
            public void onStart() {
                super.onStart();
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setMessage("Conectando . . .");
                progress.setCancelable(false);
                progress.show();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                progress.dismiss();
                Toast.makeText(getApplicationContext(), "Fallo: " + throwable.getMessage() + statusCode, Toast.LENGTH_LONG).show();

                String error = "" + statusCode + ": " + throwable.getMessage();
                if(content == IMAGES_FILE)
                    downloadedImages = false;
                else
                    downloadedSentences = false;
                upload(error);
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String responseString) {
                progress.dismiss();
                if (content == IMAGES_FILE) {
                    downloadedImages = true;
                    loadList(imagesList, responseString.split("\n"));
                }
                else {
                    downloadedSentences = true;
                    loadList(sentecesList, responseString.split("\n"));
                }

            }
        });
    }

    public void upload(String line) {
        final ProgressDialog progreso = new ProgressDialog(MainActivity.this);
        RequestParams params = new RequestParams();
        Date date = new Date();
        line += " || date: " + date.toString();

        params.put("newLine", line);
        params.put("password", "1234");

        RestClient.post(ERRORS, params, new TextHttpResponseHandler() {
            @Override
            public void onStart() {
                progreso.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progreso.setMessage("Conectando . . .");
                progreso.setCancelable(false);
                progreso.setOnCancelListener(new DialogInterface.OnCancelListener() {
                    public void onCancel(DialogInterface dialog) {
                        RestClient.cancelRequests(getApplicationContext(), true);
                    }
                });
                progreso.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, String response) {
                progreso.dismiss();
                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, String response, Throwable t) {
                Toast.makeText(getApplicationContext(), response, Toast.LENGTH_LONG).show();
                sentence.setText(response);
                progreso.dismiss();
            }
        });

    }

    public void loadList(ArrayList<String> list, String[] array) {

        for (int i = 0; i < array.length; i++) {
            list.add(array[i]);
        }
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }


    private class MyCountDownTimer extends CountDownTimer {


        private MyCountDownTimer() {
            super(Integer.valueOf(input.readRaw(INTERVAL)) * 1000, 1);
            Picasso.with(getApplicationContext())
                    .load(imagesList.get(index))
                    .placeholder(R.drawable.wait)
                    .error(R.drawable.error)
                    .resize(1024, 512)
                    .into(img);
            sentence.setText(sentecesList.get(index));
        }

        @Override
        public void onTick(long millisUntilFinished) {

        }

        @Override
        public void onFinish() {
            index++;
            if (index < imagesList.size())
                new MyCountDownTimer().start();
            else {
                index = 0;
                running = false;
            }


        }
    }

}
